package com.stack.util;

import com.stack.constants.HelperConstant;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.zip.GZIPInputStream;

public class Utility {

    public static String makeEndPointRequest(String input) throws IOException {
        URL url = new URL("https://api.stackexchange.com/2.2/search?order=desc&sort=activity&intitle="+URLEncoder.encode(input, "UTF-8")+"&site=stackoverflow");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "*/*");
        conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
        conn.setRequestProperty("Connection", "keep-alive");
        if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new IOException("Failed : HTTP error code : " + conn.getResponseCode());
        }
        InputStreamReader inputStream = getInputStreamReader(conn);
        BufferedReader bufferedReader = new BufferedReader(inputStream);
        StringBuilder responseBuidler = new StringBuilder();
        String responseLine;
        while ((responseLine = bufferedReader.readLine()) != null) {
            responseBuidler.append(responseLine);
        }
        return responseBuidler.toString();
    }

    private static InputStreamReader getInputStreamReader(HttpURLConnection httpURLConnection) {
        String contentEncodingHeader = httpURLConnection.getHeaderField("Content-Encoding");
        InputStreamReader inputStreamReader = null;
        try {
            if (contentEncodingHeader != null && contentEncodingHeader.equalsIgnoreCase("gzip")) {
                GZIPInputStream gzis = new GZIPInputStream(httpURLConnection.getInputStream());
                inputStreamReader = new InputStreamReader(gzis);
            } else {
                inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
            }
        } catch (IOException ex) {
            System.out.println(HelperConstant.ERR_MSG + ex);
        }
        return inputStreamReader;
    }
}
