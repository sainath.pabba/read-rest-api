package com.stack;

import com.stack.constants.HelperConstant;
import com.stack.util.Utility;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StackExchange {

    public static void main(String[] args) {
        try {
            System.out.print(HelperConstant.QUERY_MSG); //neet to use logger
            //Enter data using BufferReader
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            serviceProcess(reader);
        } catch (IOException e) {
            System.out.println(HelperConstant.ERR_MSG + e);
        }
    }

    private static void serviceProcess(BufferedReader reader) throws IOException {
        for (int i = 0; i < 1; i++) {
            String inputString = reader.readLine();
            if (null != inputString && HelperConstant.EMPTY_STRING != inputString && !HelperConstant.EXIT_INPUT.equalsIgnoreCase(inputString)) {
                i--;
                String jsonString = Utility.makeEndPointRequest(inputString);
                JSONObject jsonObject = new JSONObject(jsonString);
                JSONArray jsonArray = jsonObject.getJSONArray(HelperConstant.ITEM_API_VALUE);
                if (jsonArray.length() > 0) {
                    for (int index = 0; index < (jsonArray.length() > 4 ? 5 : jsonArray.length()); index++) {
                        StringBuilder stringBuilder = new StringBuilder("\n");
                        String jsonIndex = jsonArray.get(index).toString();
                        JSONObject singleObj = new JSONObject(jsonIndex);
                        stringBuilder.append(HelperConstant.DISPLAY_TITLE_KEY).append(singleObj.get(HelperConstant.API_VALUE_TITLE_KEY)).append("\n");
                        stringBuilder.append(HelperConstant.DISPLAY_LINK_KEY).append(singleObj.get(HelperConstant.API_VALUE_LINK_KEY)).append("\n");
                        JSONObject owner = new JSONObject(singleObj.get(HelperConstant.OWNER_OBJECT_KEY).toString());
                        stringBuilder.append(HelperConstant.OWNER_DISPLAY_NAME_KEY).append(owner.get(HelperConstant.API_VALUE_OWNER_DISPLAY_NAME_KEY));
                        System.out.println(stringBuilder.toString());
                    }
                } else {
                    System.out.println(HelperConstant.NO_RECORD_MSG + "\n");
                }
                System.out.print("\n" + HelperConstant.QUERY_MSG);
            } else {
                System.out.println(HelperConstant.THANKYOU);
            }
        }
    }
}
