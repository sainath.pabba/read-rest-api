package com.stack.constants;

public class HelperConstant {
    public static final String QUERY_MSG = "Please enter your search query: ";
    public static final String NO_RECORD_MSG = "No Records available on searched query";
    public static final String ERR_MSG = "Something went wrong...";

    public static final String ITEM_API_VALUE = "items";

    public static final String DISPLAY_TITLE_KEY = "Title: ";
    public static final String API_VALUE_TITLE_KEY = "title";

    public static final String DISPLAY_LINK_KEY = "URL: ";
    public static final String API_VALUE_LINK_KEY = "link";

    public static final String OWNER_OBJECT_KEY = "owner";
    public static final String OWNER_DISPLAY_NAME_KEY = "Author Display Name: ";
    public static final String API_VALUE_OWNER_DISPLAY_NAME_KEY = "display_name";

    public static final String EXIT_INPUT = "EXIT";
    public static final String EMPTY_STRING = "";

    public static final String THANKYOU = "Thank you for your time!";
}
