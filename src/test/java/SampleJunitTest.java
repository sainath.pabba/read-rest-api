import com.stack.StackExchange;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class SampleJunitTest {


    @Test
    public void testMain() {
        System.out.println("main");
        final InputStream original =  new ByteArrayInputStream("JAVA\nEXIT".getBytes());
        System.setIn(original);
        StackExchange.main(null);
    }
}
